Git Basics

Hello World

====================================
CLI (Command Line Interface) Command
====================================

	pwd (Present Working Directory) - shows the current folder we are working on

	mkdir <directory name> - create new directory

	cd <folderName/path to folder> - change directory

	cd .. - up one folder

	ls - list files/folder contained in the current directory

	touch <file name> - create new file
	
	clear - clear previous commands in terminal

	arrow up - show last command

=====================================
How to Generate SSH Key
=====================================
1. Create an SSH key.
	Terminal/GitBash

		ssh-keygen

		Important Note:
			- After triggering the command the user will be prompted to choose a file location on where to store the SSH key in their device. Just press "Enter" to use the default location.
			- After declaring where the SSH key will be stored, the user will be prompted to add a "passphrase" which will act as the password when using the git account with the associated SSH key. Just press "Enter" again to leave the passphrase empty for ease of access.

2. Copy the SSH key.
	Terminal/GitBash

		Linux
			xclip -sel clip < ~/.ssh/id_rsa.pub

		Mac
			pbcopy < ~/.ssh/id_rsa.pub

		Windows
			cat ~/.ssh/id_rsa.pub | clip

		Important Note:
			- The following commands will copy the contents of the "id_rsa.pub" file located inside the ".ssh" folder in the clipboard.
			- If triggering a command returns an error, check if the path is correct and if the file exists.

			- Alternatively, we can navigate to the ".ssh" folder and locate the "id_rsa.pub" file and open it with our text editors to manually copy the contents.

			- The ".ssh" folder is a hidden folder, go to the hidden files and folders tab in your files explorer.
			- For Linux users, an error might be encountered xclip is not recognized as an internal or external command. Install xclip using the following command:
				- sudo apt-get update -y
				- sudo apt-get install -y xclip

3. Add the generated SSH key to git.
	Browser > GitLab/GitHub

=====================================
Basic Git Commands
=====================================

git init - initialize a folder as a local repository

git status - display files that are ready to added and committed
			- red text means files are not staged
			- green text means files are staged and ready to commit

git add . (or -A) - allows to track all the changes that we made and preparte these files as a new version to be uploaded

git commit -m "<commitMessage>" - creates a new commit or version of our files to be pushed into our remote repository

git remote add <alias> <remote_repo_link> - to add/connect a remote repository to our local repository

git remote remove <alias> - remove exisiting remote repo (ssh link)

git push <alias> <branchName(master/main)> - upload all committed changes in the local repository

git clone <remote_repo_link> - clone/duplicate a remote repository and its contents inside our machines/device

git pull <alias> master/main - pull or get the updates from a remote repo to a local repo


==========================================
Git Config
==========================================

These commands will allow us to have git recognize the person trying to push into an online/remote repo:

git config --global user.email "email@mail.com" - configure the email used to push into the remote repo.

git config --global user.name "my.username" - configure the name/username of the user trying to push into gitlab.

git config --global --list - check the configuration list

===============================
